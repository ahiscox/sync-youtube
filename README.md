# ABOUT SYNC-YOUTUBE #

This is a simple little HTML5 application, and Node.js server, that allows two people to watch YouTube videos in sync with each other over the Internet.

### How does it work? ###
At least one user must run the Node.js server, and have it open to the Internet. Each user then connects to that server, which serves up the HTML, Javascript and CSS required for the app. The server is also responsible for handling socket.io websocket events, which is how the browsers handle sharing control over the video.

### Requirements ###
This has only been tested on Google Chrome v47 and Android Chrome browser. It requires a browser that supports websockets through socket.io. The server is cross platform and can run on any device that supports Node.js and Express package.

### How to use it? ###
#### Server ####
By default the server starts on port 3000, this can be configured by 
editing index.js; to start the server, open a command prompt and navigate to the sync-youtube folder then type:

```
#!bash

C:\Path\To\sync-youtube> node index.js
```

#### Client ####
Connecting as a client is simple, visit http://server-ip:3000/ and everything will be loaded automatically (where server-ip is, put the IP address of your Internet accessible server). 

### What are the controls? ###
Playback controls are shown at the bottom of the main page. 

#### Play/Pause ####
Play/Pause are sent across the network to the other user(s). Every time a video is paused, all other players update with the current playback location. On Play, an event is fired from the server requesting all clients begin playing. 
#### Seek and Reset ####
At times you will want to watch a video, while having a phone conversation with the other user. In this case the videos might be slightly out of sync, and lead to irritating echoing. The seek buttons (<<<,<<,>>,>>>) and RESET button are used to control the speed of playback. By controlling the speed while listening to another users video over the phone, you can fine tune the video until the two are perfectly in sync. The <<< and >>> set playback speed to 1/4 and 1-1/2 times respectively. The << and >> set playback speed to 1/2 and 1-1/4 times respectively. And the reset button resets playback speed to regular speed.



### Can we have more than two people? ###
Technically yes. However getting everyone users playback in sync would probably be a nightmare. If you wish to try, do so at your own risk and sanity.