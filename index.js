var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use( '/static', express.static( __dirname + '/static' ) );

app.get('/', function(req, res){
  	res.sendFile('index.html', { root: __dirname });
});

io.on('connection', function(socket){
	socket.on('player-paused', function(msg) {
		// Every time player is paused, sync all videos
		socket.broadcast.emit('player-sync', msg);
	});

	socket.on('player-state', function(msg) {
		console.log('player state changed: ' + msg );
	});

	socket.on('player-start', function(msg) {
		io.emit('player-play', Date.now());
	});
});

http.listen(3000, function(){
  	console.log('listening on *:3000');
});