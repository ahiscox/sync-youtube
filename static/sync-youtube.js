var socket = io();

var player;
function onYouTubeIframeAPIReady() {
	player = new YT.Player('player', {
		height: '390',
		width: '640',
		controls: 0,
		videoId: 'M7lc1UVf-VE',
		events: {
			'onReady': onPlayerReady,
			'onStateChange': onPlayerStateChange
		}
	});
}


function onPlayerReady(event) {
	// event.target.playVideo();
	socket.emit('player-state', 'ready');

  	// Handle play/pause button
  	$('#button-play').on('click', function(e) {
  		if (player.getPlayerState() == YT.PlayerState.PLAYING) {
  			player.pauseVideo();
  		} else {
  			socket.emit('player-start', '1');
  		}
  	});

	// Handle fullscreen mode
	$('#button-fullscreen').on('click', function(e){
		var container = document.getElementById('player-container');
		container.webkitRequestFullScreen();
	});

	$(document).on('webkitfullscreenchange', function(e) {
		if (!document.webkitIsFullScreen) {
			$('#player-container').removeClass('fullscreen');
		} else {
			$('#player-container').addClass('fullscreen')
		}
	});

    // Handle skipping controls
    $('.button-skip').on('click', function(e){
    	var seek = function(time, forward) {
    		if (forward) {
    			player.seekTo( player.getCurrentTime() + time);
    		} else {
    			player.seekTo( player.getCurrentTime() - time);
    		}
    	};

    	switch (e.target.name) {
    		case 'left-fast':
    		player.setPlaybackRate(0.25);
    		break;

    		case 'left-slow':
    		player.setPlaybackRate(0.5);
    		break;

    		case 'right-slow':
    		player.setPlaybackRate(1.25);
    		break;

    		case 'right-fast':
    		player.setPlaybackRate(1.5);
    		break;

    		case 'reset-playback':
    		player.setPlaybackRate(1);
    		break;

    		default:
    		console.error('Unknown Target Name');
    	}
    });

	// Setup Timeline
	var timelineCurrentTime;
	$('#timeline').attr('max', player.getDuration());
	$('#timeline').on('change', function() {
		player.seekTo( $(this).val());
	});

	$('#player-overlay').on('click', function(e){
		console.log('clicked');
		player.playVideo();
		player.pauseVideo();
		player.seekTo(0);
	});
}

var timelineTimer;
function onPlayerStateChange(event) {
	if ( event.data == YT.PlayerState.PAUSED ) {
		socket.emit('player-paused', player.getCurrentTime());
		$('#button-play').attr('value', 'Play');
	}

	if ( event.data == YT.PlayerState.PLAYING ) {
		var timelineEl = document.getElementById('timeline');
		timelineTimer = setInterval(function() {
			timelineEl.value = player.getCurrentTime();
		}, 1000);
		$('#button-play').attr('value', 'Pause');
	}

	if ( event.data != YT.PlayerState.PLAYING ) {
    	// If not playing then kill updating timer.
    	clearInterval( timelineTimer ); 
	}
}


// Handle socket.io events
socket.on('player-sync', function(msg) {
	// Sync position every time we pause.
	player.pauseVideo();
	player.seekTo( msg );
});

socket.on('player-play', function(time) {
	player.playVideo();
});
